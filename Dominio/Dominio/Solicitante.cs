﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Dominio
{
   public class Solicitante : Usuario
    {
        public bool SolicitudesPendientes { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNac { get; set; }
        public string Celular { get; set; }
        public List<Proyecto> ListaProyectos { get; set; }

        //Usando regex verifica que formato 09X YYYYY siendo X un numero del 1 al 9, Y un numero del 0 al 9 y forzando a dejar un espacio en blanco
        //Retorna true si el formato del celular es correcto
        public static bool ValidarFormatoCelular(string celular)
        {
            var input = celular;
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }
            var formatoCeluluar = new Regex("(09[1-9] {1}[0-9]{6})");

            if (formatoCeluluar.IsMatch(input))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Usando regex verifica que el mail tenga el formato correxto texto@texto.texto
        //Retorna true si el formato del mail es correcto
        public static bool ValidarFormatoEmail(string mail)
        {
            var input = mail;
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }
            var formatoEmail = new Regex("([a-z0-9!#$%&'*+/=?^_`{|}~-])+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

            if (formatoEmail.IsMatch(input))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Usando regex verifica que la password sea correcta que contenga al menos 1 caracter mayuscula, al menos 1 miniscula y al menos un numero
        //Retorna true si el formato de la password es correcta
        public static bool ValidarFormatoPassword(string pass)
        {
            var input = pass;
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }
            var formatoPassword = new Regex("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}");

            if (formatoPassword.IsMatch(input))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Calcula la edad de la persona segun su fecha de nacimiento
        //Retorna un int con la edad
        public static int CalcularEdad(DateTime fechaNacimiento)
        {

            int edad = 0;

            edad = DateTime.Now.Year - fechaNacimiento.Year;

            if (DateTime.Now.Month < fechaNacimiento.Month || DateTime.Now.Month == fechaNacimiento.Month && DateTime.Now.Day < fechaNacimiento.Day)
            {
                edad--;
            }

            return edad;
        }

        //Recibe la edad y te dice si la persona es mayor a 18 o no
        //Si la persona tiene 21 años o mas retorna true
        public static bool EdadMayorA21(DateTime fechaNacimiento) {
           
            bool ret = false;

            //int edadUser = CalcularEdad(fechaNacimiento);

            int edadUser = CalcularEdad(fechaNacimiento);

            if (edadUser >= 21) {
                ret = true;
            }

            return ret;
        }

        //Verifica que dos campos contengan el mismo string
        //Si el string de ambos campos es igual reotrna true
        public static bool ContraYVerificarContraSonIguales(string pass1, string pass2) 
        {
            bool ret = false;

            if(pass1 == pass2)
            {
                ret = true;
            }

            return ret;
        }

        //Metodo para agregar un proyecto a la lista
        //GONZA
        public bool AgregarProyecto(Proyecto proyecto)
        {
            bool exito = false;
            if (proyecto != null)
            {
                ListaProyectos.Add(proyecto);
                exito = true;
            }
            return exito;
        }

    }

}