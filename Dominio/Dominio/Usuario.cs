﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Runtime.Remoting.Messaging;

namespace Dominio
{
    public class Usuario 
    {
        public int Id { get; set; }
        public int Cedula { get; set; } 
        public string Pass { get; set; }
        public string Tipo { get; set; }

        //Recibe una cedula y retorna true si contiente entre 7 y 8 caracteres 
        //Solo hay ciudadanos uruguayos vivos con CI entre 7 y 8 digitos (sin espacios ni guiones)
        public static bool ValidarCedula(int cedulaUsuario) 
        {
            bool ret = false;

            string cedu = cedulaUsuario.ToString();

            if (cedu.Length >= 7 && cedu.Length <= 8) 
            {
                ret = true;
            }

            return ret;
        }
    }
}
