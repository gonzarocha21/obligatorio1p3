﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Personal : Proyecto
    {
        public string descripcionPersonal { get; set; }

       

        public override string Tipo()
        {
            string tipo = "Personal";
            return tipo;
        }

        public override decimal CalcularTotalConIntereses()
        {
            decimal monto = montoSolicitado;
            if (monto >= montoMaximo)
            {
                decimal descuento = decimal.Multiply(montoMaximo, 0.2M);
                monto = montoMaximo - descuento;

            }

            decimal interes = monto * tasaInteres;
            montoFinalConInte = monto + interes;
            return montoFinalConInte;
        }

        public override bool AgregarSolicitante(Solicitante solicitante)
        {
            bool exito = false;
            if (solicitante != null)
            {
                this.Solicitante = solicitante;
                exito = true;
            }
            return exito;
        }
    }
}
