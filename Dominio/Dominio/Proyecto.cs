﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Dominio
{
    public abstract class Proyecto
    {
       

        public int id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public decimal montoSolicitado { get; set; }
        public int cantidadCuotas { get; set; }
        public string urlImagen { get; set; }
        public decimal tasaInteres { get; set; }
        public string estado { get; set; }
        public decimal montoMaximo { get; set; } 
        public DateTime fechaPresentacion { get; set; }
        public decimal montoCuota { get; set; }
        public decimal montoFinalConInte { get; set; }
        public string tipo { get; set; }
        public Solicitante Solicitante { get; set; }

        // hago la carga de estado
        public string Estado() {
            estado = "Pendiente de revisión";
            return estado;
        }
        
        public abstract string Tipo();
       
        public abstract decimal CalcularTotalConIntereses();

        public decimal CalcularMontoCuota()
        {
            CalcularTotalConIntereses();
            montoCuota = montoFinalConInte / cantidadCuotas;
            return montoCuota;
        }

        //Gonza
        public abstract bool AgregarSolicitante(Solicitante solicitante);

    }
}
