﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Cooperativo : Proyecto
    {
        public int cantIntegrantes { get; set; }

        public override decimal CalcularTotalConIntereses()
        {
            decimal montoMaximoLocal = montoMaximo;
            decimal monto = montoSolicitado;
            decimal incremento;
            if (cantIntegrantes > 10)
            {
                incremento = montoMaximo * 0.2M;
                montoMaximoLocal = montoMaximo + incremento;
            }
            else {
                incremento = montoMaximo * (cantIntegrantes * 0.02M);
                montoMaximoLocal = montoMaximo + incremento;
            }

            if (montoSolicitado > montoMaximoLocal)
            {
                monto = montoMaximoLocal;
            }
            else {
                monto = montoSolicitado;
            }
            
           
            decimal interes = monto * tasaInteres;
            montoFinalConInte = monto + interes;
           
            return montoFinalConInte;
        }

        public override string Tipo()
        {
            string tipo = "Cooperativo";
            return tipo;
        }

        public override bool AgregarSolicitante(Solicitante solicitante)
        {
            bool exito = false;
            if (solicitante != null)
            {
                this.Solicitante = solicitante;
                exito = true;
            }
            return exito;
        }
    }
}
