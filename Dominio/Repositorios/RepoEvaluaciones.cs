﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Repositorios
{
    //COPIAR TODO ESTO
    class RepoEvaluaciones : Repositorio, IRepositorio<EvaluacionProyecto>
    {
        public bool Alta(EvaluacionProyecto obj)
        {
            //int puntaje = obj.Puntaje;
            //DateTime fechaEvaluacion = obj.FechaEvaluacion;
            //Usuario Usuario = obj.Usuario;
            //Proyecto Proyecto = obj.Proyecto;

            bool ret = false;

            /*
             INSERT INTO EvaluacionProyectos (Puntaje, FechaEvalucion, Cedula_Admin, Id_Admin, Id_Proyecto) 
                VALUES (10, '2020-11-21', 45773333, 3, 1);
                GO
             */
            SqlConnection con = ObtenerConexion();

            string sql = "insert into EvaluacionProyectos(Puntaje, FechaEvalucion, Cedula_Admin, Id_Admin, Id_Proyecto) " +
                                      "values(@puntaje, @fecha, @cedula_admin, @id_admin, @id_proy);";

            SqlCommand com = new SqlCommand(sql, con);


            com.Parameters.AddWithValue("@puntaje", obj.Puntaje);
            com.Parameters.AddWithValue("@fecha", obj.FechaEvaluacion);
            com.Parameters.AddWithValue("@cedula_admin", obj.Usuario.Cedula);
            com.Parameters.AddWithValue("@id_admin", obj.Usuario.Id);
            com.Parameters.AddWithValue("@id_proy", obj.Proyecto.id);

            try
            {
                con.Open();
                int afectadas = com.ExecuteNonQuery();
                con.Close();

                ret = afectadas == 1;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return ret;


        }

        public bool Baja(int id)
        {
            throw new NotImplementedException();
        }

        public EvaluacionProyecto BuscarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public bool Modificacion(EvaluacionProyecto obj)
        {
            throw new NotImplementedException();
        }

        public List<EvaluacionProyecto> TraerTodo()
        {
            throw new NotImplementedException();
        }

        public string GenerarArchivoEvaluacionProyectos()
        {
            string retorno = "";

            SqlConnection con = ObtenerConexion();

            string sql = "select * from EvaluacionProyectos;";

            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                while (reader.Read())
                {
                    retorno += reader.GetInt32(0) + "|" + reader.GetInt32(1) + "|" + reader.GetDateTime(2).ToString() + "|" + reader.GetInt32(3) + "|" + reader.GetInt32(4) + "|" + reader.GetInt32(5) + "\n";
                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return retorno;

        }

    }
}
