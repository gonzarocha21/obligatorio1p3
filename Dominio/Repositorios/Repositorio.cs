﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace Repositorios
{
    public abstract class Repositorio
    {
        protected SqlConnection ObtenerConexion()
        {
            string strCon = ConfigurationManager.ConnectionStrings["MiConexion"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            return con;
        }

    }
}
