﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Repositorios
{
    class RepoProyecto : Repositorio, IRepositorio<Proyecto>
    {
        public bool Alta(Proyecto obj)
        {
            // juan
            ModificarSolicitudesPendiente(obj.Solicitante.Id,true);

            bool ret = false;

            SqlConnection con = ObtenerConexion();

            string sql = "insert into Proyectos(Titulo, Descripcion, MontoSolicitado, CantidadCuotas, URLImagen, TasaInteres, Estado, MontoMaximo, FechaPresentacion, MontoCuota, MontoFinalConInteres,Tipo, DescripcionPersonal,CantIntegrantes, Id_Solicitante)" +
                                      " values(@titulo, @desc, @montoSolicitado, @cantCuotas, @url, @tasaint, @estado, @montoMaximo, @fechaPresentacion, @montoCuota, @montoFinal, @tipo, @DescripcionPersonal, @CantIntegrantes, @IdSolicitante );";
            SqlCommand com = new SqlCommand(sql, con);


            com.Parameters.AddWithValue("@titulo", obj.titulo);
            com.Parameters.AddWithValue("@desc", obj.descripcion);
            com.Parameters.AddWithValue("@montoSolicitado", obj.montoSolicitado);
            com.Parameters.AddWithValue("@cantCuotas", obj.cantidadCuotas);
            com.Parameters.AddWithValue("@url", obj.urlImagen);
            com.Parameters.AddWithValue("@tasaint", obj.tasaInteres);
            com.Parameters.AddWithValue("@estado", obj.estado);
            // llamo al metodo cargar monto maximo que se encuentra en la variable global
            com.Parameters.AddWithValue("@montoMaximo",obj.montoMaximo);
            com.Parameters.AddWithValue("@fechaPresentacion", obj.fechaPresentacion);
            com.Parameters.AddWithValue("@montoCuota", obj.montoCuota);
            com.Parameters.AddWithValue("@montoFinal", obj.montoFinalConInte);
            com.Parameters.AddWithValue("@tipo", obj.tipo);
            com.Parameters.AddWithValue("@IdSolicitante", obj.Solicitante.Id);

            if (obj.tipo == "Personal")
            {
                Personal p = (Personal)obj;
                com.Parameters.AddWithValue("@DescripcionPersonal", p.descripcionPersonal);
                com.Parameters.AddWithValue("@CantIntegrantes", 0);

            }
            else
            {
                Cooperativo c = (Cooperativo)obj;
                com.Parameters.AddWithValue("@CantIntegrantes", c.cantIntegrantes);
                //com.Parameters.AddWithValue("@DescripcionPersonal", null);
                com.Parameters.AddWithValue("@DescripcionPersonal", "");

            }
            try
            {
                con.Open();
                int afectadas = com.ExecuteNonQuery();
                con.Close();

                ret = afectadas == 1;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return ret;

        }

        public bool Baja(int id)
        {
            throw new NotImplementedException();
        }

        public Proyecto BuscarPorId(int id)
        {
            Proyecto p = null; 
            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Proyectos where ProyectoID =" + id;


            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();


                while (reader.Read())
                {
                    if (reader.GetString(12) == "Personal")
                    {
                        Personal per = Auxiliar.CrearPersonal(reader);

                        p = per;

                    }

                    if (reader.GetString(12) == "Cooperativo")
                    {
                        Cooperativo coop = Auxiliar.CrearCooperativo(reader);

                        p = coop;

                    }


                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return p;
        }

        public bool Modificacion(Proyecto obj)
        {
            throw new NotImplementedException();
        }

        public List<Proyecto> TraerTodo()
        {
            List<Proyecto> proyectos = new List<Proyecto>();


            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Proyectos ";


            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();


                while (reader.Read())
                {
                    if (reader.GetString(12) == "Personal")
                    {
                        Personal per = Auxiliar.CrearPersonal(reader);

                        proyectos.Add(per);

                    }

                    if (reader.GetString(12) == "Cooperativo")
                    {
                        Cooperativo coop = Auxiliar.CrearCooperativo(reader);

                        proyectos.Add(coop);

                    }


                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return proyectos;
        }


      

      

        public decimal TraerMin() {
            SqlConnection con = ObtenerConexion();
            string sql = "SELECT minimo FROM TopesMinMax ";


            SqlCommand com = new SqlCommand(sql, con);
            decimal tasa = 0;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {
                    tasa = reader.GetDecimal(0);

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return tasa;
        }

        public decimal TraerMax()
        {
            SqlConnection con = ObtenerConexion();
            string sql = "SELECT maximo FROM TopesMinMax ";


            SqlCommand com = new SqlCommand(sql, con);
            decimal tasa = 0;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {
                    tasa = reader.GetDecimal(0);

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return tasa;
        }


        public decimal BuscarTasaDeInteresPorCuota(int cuotas)
        {
            SqlConnection con = ObtenerConexion();
            string sql = "SELECT TASA FROM TasasDeInteres where "+ cuotas +" between "+ " Desde and Hasta ";


            SqlCommand com = new SqlCommand(sql, con);
            decimal tasa = 0;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {
                    tasa = reader.GetDecimal(0);
                    
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return tasa;
        }

        //GONZA AGREGARRRR! 

        public List<Proyecto> BuscarProyectosDeUsuarioPorId(int idUsuario)
        {
            List<Proyecto> proyectosDelUsuario = new List<Proyecto>();

            SqlConnection con = ObtenerConexion();

            string sql = "SELECT * " +
                         "FROM Proyectos " +
                         "WHERE Id_Solicitante = " + idUsuario;

            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetString(12) == "Personal")
                    {
                        Personal per = Auxiliar.CrearPersonal(reader);
                        proyectosDelUsuario.Add(per);
                    }

                    if (reader.GetString(12) == "Cooperativo")
                    {
                        Cooperativo coop = Auxiliar.CrearCooperativo(reader);
                        proyectosDelUsuario.Add(coop);
                    }
                }

                con.Close();
            }
            catch
            {
                //throw;
                //Que en el caso de que no exista un proyecto con el ID del usuario devolvemos lista vacia
                return proyectosDelUsuario;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return proyectosDelUsuario;
        }



        // juan 
        public List<Proyecto> BuscarProyectosPorFiltro(int cedula, DateTime fecha, string estado, string texto)
        {
            List<Proyecto> proyectos = new List<Proyecto>();
            string consultaCedula = "";
            string consultaFecha = "";
            string consultaEstado = "";
            string consultaTexto = "";
            SqlConnection con = ObtenerConexion();

            if (cedula > 0)
            {
                consultaCedula = "and u.cedula=" + cedula;
            }
            if (fecha != DateTime.MinValue)
            {
                string f = fecha.ToString("yyyy/MM/dd");
                consultaFecha = "and p.FechaPresentacion = " +"'" + f + "'";
            }
            if (estado != "")
            {
                consultaEstado = "and p.Estado = " + "'" + estado +"'";
            }
            if (texto != "")
            {
                consultaTexto = "and p.Titulo LIKE" + "'%" + texto + "%'" + " OR " + "p.Descripcion LIKE" + "'%" + texto + "%'";

            }


            string sql = "select distinct p.proyectoid ,p.Titulo, p.Descripcion,p.MontoSolicitado, p.CantidadCuotas, p.URLImagen, " +
                "p.TasaInteres, p.estado, p.MontoMaximo, p.FechaPresentacion, p.MontoCuota, p.MontoFinalConInteres,p.Tipo," +
                " p.DescripcionPersonal,p.CantIntegrantes, p.Id_Solicitante from Proyectos p, Solicitantes s ," +
                " Usuarios u where s.Id = u.Id and p.Id_Solicitante = s.Id " + consultaCedula + consultaEstado + consultaFecha + consultaTexto + ";";


            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();


                while (reader.Read())
                {
                    if (reader.GetString(12) == "Personal")
                    {
                        Personal per = Auxiliar.CrearPersonal(reader);

                        proyectos.Add(per);

                    }

                    if (reader.GetString(12) == "Cooperativo")
                    {
                        Cooperativo coop = Auxiliar.CrearCooperativo(reader);

                        proyectos.Add(coop);

                    }


                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return proyectos;
        }

        public bool CambiarEstadoProyecto(string estado, int idProyecto)
        {
            bool ret = false;

            SqlConnection con = ObtenerConexion();

            string sql = "UPDATE Proyectos " +
                         "SET Estado=@estado " +
                         "WHERE ProyectoID = @idProyecto;";

            SqlCommand com = new SqlCommand(sql, con);

            com.Parameters.AddWithValue("@estado", estado);
            com.Parameters.AddWithValue("@idProyecto", idProyecto);

            try
            {
                con.Open();
                int afectadas = com.ExecuteNonQuery();
                con.Close();

                ret = afectadas == 1;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return ret;
        }

        // juan 10.10
        public bool ModificarSolicitudesPendiente(int id, bool valor)
        {
            SqlConnection con = ObtenerConexion();
            bool ret = false;
           
            bool Valor = valor;
            int ValorInt = Convert.ToInt32(Valor);

            string sql = "update Solicitantes set SolicitudesPendientes = " + ValorInt + " where Solicitantes.Id = " + id;

            SqlCommand com = new SqlCommand(sql, con);
            
            try
            {
                con.Open();
                int afectadas = com.ExecuteNonQuery();
                con.Close();

                ret = afectadas == 1;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return ret;
        }
    }

        
}

