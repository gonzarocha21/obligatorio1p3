﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using Repositorios;

namespace Repositorios
{
    public class FachadaPrestamos
    {
        
        
        public static List<Solicitante> TraerTodosLosSolicitantes()
        {
            RepoUsuarios repoUsuarios = new RepoUsuarios();
            return repoUsuarios.TraerTodosLosSolicitantes();
        }

        public static List<Usuario> TraerTodosLosAdmins()
        {
            RepoUsuarios repoUsuarios = new RepoUsuarios();
            return repoUsuarios.TraerTodosLosAdmins();
        }



        public static List<Proyecto> TraerTodosLosProyectos()
        {
            RepoProyecto repoProye = new RepoProyecto();
            return repoProye.TraerTodo();
        }

        public static List<Proyecto> TraerTodosLosProyectosPendientes()
        {
            RepoProyecto repoProye = new RepoProyecto();
            List<Proyecto> proyectosPendientes = new List<Proyecto>();
            List<Proyecto> proyectos = repoProye.TraerTodo();
            foreach (Proyecto p in proyectos)
            {
                if (p.estado == "Pendiente de revisión")
                {
                    proyectosPendientes.Add(p);
                }
            }
            return proyectosPendientes;
        }

        public static List<Proyecto> BuscarProyectosPorFiltro(int cedula, DateTime fecha, string estado, string texto)
        {
            List<Proyecto> proyectos = new List<Proyecto>();
            RepoProyecto repoProye = new RepoProyecto();
            return repoProye.BuscarProyectosPorFiltro(cedula, fecha, estado, texto);
        }

        public static bool AltaUsuario(int cedula, string pass, string tipo)
        {
            bool ret = false;
            Usuario usu = new Usuario()
            {
                Cedula = cedula,
                Pass = pass,
                Tipo = tipo
            };

            RepoUsuarios repoU = new RepoUsuarios();

            ret = repoU.Alta(usu);
            return ret;
        }

        public static Usuario BuscarUsuPorID(int id)
        {


            RepoUsuarios repoU = new RepoUsuarios();

            Usuario usu = repoU.BuscarPorId(id);

            return usu;


        }

        public static bool BajaUsuario(int id)
        {
            bool ret = false;

            RepoUsuarios repoU = new RepoUsuarios();

            ret = repoU.Baja(id);
            return ret;
        }

        public static bool AltaSolicitante(int cedula, string nombre, string apellido, DateTime fechaNac, string celular, string email, string pass, string tipo, bool solicitudesPendientes, List<Proyecto> listaProyectos)
        {
            bool ret = false;
            Solicitante soli = new Solicitante()
            {
                Cedula = cedula,
                Nombre = nombre,
                Apellido = apellido,
                FechaNac = fechaNac,
                Celular = celular,
                Pass = pass,
                Email = email,
                Tipo = tipo,
                SolicitudesPendientes = solicitudesPendientes,
                ListaProyectos = listaProyectos
            };

            RepoUsuarios repoU = new RepoUsuarios();

            ret = repoU.Alta(soli);
            return ret;
        }

        public static Usuario buscarUsuarioPorCedula(int cedula) {

            RepoUsuarios repoU = new RepoUsuarios();

            Usuario usu = repoU.BuscarPorCedula(cedula);

            return usu;

        }

        public static Solicitante buscarSolicitantePorCedula(int cedula)
        {

            RepoUsuarios repoU = new RepoUsuarios();

            Solicitante soli = repoU.BuscarSolicitantePorCedula(cedula);

            return soli;

        }

        public static Solicitante buscarSoliporID(int id)
        {


            RepoUsuarios repoU = new RepoUsuarios();
            //creo un solicitante 
            Solicitante soli = null;
            // creo un usuario con la llamada a BuscoPorId 
            Usuario u = repoU.BuscarPorId(id);

            if (u is Solicitante) {
             soli = (Solicitante)u;
            }

            return soli;

        }
        //Juan 10.10
        public static bool altaProyectoPersonal(string titulo, string descripcion, decimal montoSolicitado, 
            int cantidadCuotas, string urlImagen, string Estado, DateTime fechaPresentacion, 
            int idSolicitante, string descripcionPersonal,string tipo, decimal tasaInteres, decimal MontoFinalConInte,decimal MontoMaximo,decimal MontoCuota) {
            
            bool ret = false;

            Usuario u = BuscarUsuPorID(idSolicitante);

            if (u is Solicitante) {
                Solicitante solicitante = (Solicitante)u;
                Personal proyP = new Personal()
                {
                    titulo = titulo,
                    descripcion = descripcion,
                    montoSolicitado = montoSolicitado,
                    cantidadCuotas = cantidadCuotas,
                    urlImagen = urlImagen,
                    tasaInteres = tasaInteres,
                    fechaPresentacion = fechaPresentacion,
                    Solicitante = solicitante,
                    tipo = tipo,
                    descripcionPersonal = descripcionPersonal,
                    montoFinalConInte = MontoFinalConInte,
                    estado = Estado,
                    montoCuota = MontoCuota,
                    montoMaximo = MontoMaximo

                };

                if (solicitante.SolicitudesPendientes == false) {
                    RepoProyecto repoP = new RepoProyecto();
                    ret = repoP.Alta(proyP);
                }



                return ret;
            }
            
            return ret;
        }
        //Juan 10.10
        public static bool altaProyectoCooperativo(string titulo, string descripcion, decimal montoSolicitado,
            int cantidadCuotas, string urlImagen, string Estado, DateTime fechaPresentacion,
            int idSolicitante, int cantIntegrantes,string Tipo, decimal tasaInteres, decimal MontoFinalConInte, decimal MontoMaximo, decimal MontoCuota)
        {

            bool ret = false;

            Usuario u = BuscarUsuPorID(idSolicitante);

            if (u is Solicitante)
            {
                Solicitante solicitante = (Solicitante)u;
                Cooperativo proyC = new Cooperativo()
                {
                    titulo = titulo,
                    descripcion = descripcion,
                    montoSolicitado = montoSolicitado,
                    cantidadCuotas = cantidadCuotas,
                    urlImagen = urlImagen,
                    tasaInteres = tasaInteres,
                    fechaPresentacion = fechaPresentacion,
                    Solicitante = solicitante,
                    cantIntegrantes = cantIntegrantes,
                    montoCuota = MontoCuota,
                    montoFinalConInte = MontoFinalConInte,
                    montoMaximo = MontoMaximo,
                    estado = Estado,
                    tipo = Tipo
                };


                if (solicitante.SolicitudesPendientes == false)
                {
                    RepoProyecto repoP = new RepoProyecto();
                    ret = repoP.Alta(proyC);
                }



                return ret;

            }

            return ret;
        }

        public static bool ActualizarSolicitante(int id, int cedula, string nombre, string apellido, DateTime fechaNac, string celular, string email, string pass, string tipo, bool solicitudesPendientes)
        {
            bool ret = false;
            Solicitante soli = new Solicitante()
            {
                Id = id,
                Cedula = cedula,
                Nombre = nombre,
                Apellido = apellido,
                FechaNac = fechaNac,
                Celular = celular,
                Pass = pass,
                Email = email,
                Tipo = tipo,
                SolicitudesPendientes = solicitudesPendientes,
                //ListaProyectos = listaProyectos
                //SAQUE LA LISTA DE PROYECTOS DE ACA, NO ESTOY SEGURO SI DEJARLA O NO
            };

            RepoUsuarios repoU = new RepoUsuarios();

            ret = repoU.Modificacion(soli);
            return ret;
        }
        public static bool ActualizarUsuario(int id, int cedula, string pass, string tipo)
        {
            bool ret = false;
            Usuario usu = new Usuario()
            {
                Id = id,
                Cedula = cedula,
                Pass = pass,
                Tipo = tipo,
            };

            RepoUsuarios repoU = new RepoUsuarios();

            ret = repoU.Modificacion(usu);
            return ret;
        }

        public static decimal BuscarTasaDeInteresPorCuota(int cuotas) {

            decimal tasa = 0;
            RepoProyecto repoP = new RepoProyecto();
            tasa = repoP.BuscarTasaDeInteresPorCuota(cuotas);

            return tasa;
        }

        //GONZA AGREGAR
        public static List<Proyecto> listaDeProyectosDeUnCliente(int id_usuario) 
        {
            List<Proyecto> proyectosDelUsuario = new List<Proyecto>();

            RepoProyecto repoP = new RepoProyecto();

            proyectosDelUsuario = repoP.BuscarProyectosDeUsuarioPorId(id_usuario);

            return proyectosDelUsuario;
        }

        public static Solicitante BuscarSolicitanteYSusProyectos(int id_usuario) 
        {
            Solicitante soli = (Solicitante)BuscarUsuPorID(id_usuario);
            List<Proyecto> proyectosDelUsuario = listaDeProyectosDeUnCliente(id_usuario);

            if (proyectosDelUsuario.Count > 0) 
            {
                foreach (Proyecto proyecto in proyectosDelUsuario)
                {
                    soli.AgregarProyecto(proyecto);
                }    
            }
            return soli; 
        }

        public static bool CambiarEstadoProyecto(string estado, int idProyecto)
        {
            RepoProyecto repoProy = new RepoProyecto();
            return repoProy.CambiarEstadoProyecto(estado, idProyecto);
        }

        public static Proyecto buscarProyectoPorId(int id) 
        {
            RepoProyecto repoProy = new RepoProyecto();

            Proyecto proyecto = repoProy.BuscarPorId(id);

            if (proyecto.tipo == "Personal")
            {
                Personal personal = (Personal)proyecto;
                return personal;
            }
            else 
            {
                Cooperativo cooperativo = (Cooperativo)proyecto;
                return cooperativo;
            }

        }


        // JUAN ver si se llama desdefachada quizas no sea necesario que este aca
        public static bool ModificarSolicitudesPendiente(int id, bool valor) {
            bool ret = false;
            if (id >= 0) {
                RepoProyecto repoP = new RepoProyecto();
                ret = repoP.ModificarSolicitudesPendiente(id, valor);
            }

            return ret;
        }

        public static decimal TraerMin() {
            RepoProyecto repoP = new RepoProyecto();
           return repoP.TraerMin();
        }

        public static decimal TraerMax()
        {
            RepoProyecto repoP = new RepoProyecto();
            return repoP.TraerMax();
        }


        public static bool SolicitudesPendientes(int id) {
            RepoUsuarios repoUsuarios = new RepoUsuarios();

            return repoUsuarios.SolicitudesPendientes(id);
        }

    }
}


