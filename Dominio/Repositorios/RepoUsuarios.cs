﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Repositorios
{
    public class RepoUsuarios : Repositorio, IRepositorio<Usuario>
    {
        public bool Alta(Usuario obj)
        {
            bool ret = false;

            if (obj != null)
            {
                SqlConnection con = ObtenerConexion();

                string sql_producto = "insert into Usuarios (Cedula, Pass, Tipo) values(@cedula, @pass, @tipo );";
                sql_producto += "select cast(scope_identity() as int);";

                SqlCommand comando_insercion_usuario = new SqlCommand(sql_producto, con);

                comando_insercion_usuario.Parameters.AddWithValue("@cedula", obj.Cedula);
                comando_insercion_usuario.Parameters.AddWithValue("@pass", obj.Pass);
                comando_insercion_usuario.Parameters.AddWithValue("@tipo", obj.Tipo);
                SqlTransaction trans = null;

                try
                {
                    con.Open();
                    trans = con.BeginTransaction(); // 1) INICIAR / OBTENER LA TRANSACCIÓN
                    comando_insercion_usuario.Transaction = trans; // 2) ASIGNARLA AL COMANDO

                    //INSERTAR EN TABLA PRODUCTO Y OBTENER EL ID AUTOGENERADO
                    int id_generado = (int)comando_insercion_usuario.ExecuteScalar(); //3)a) EJECUTO EL COMANDO 

                    SqlCommand comando_insercion_solicitante = new SqlCommand();
                    comando_insercion_solicitante.Connection = con;
                    comando_insercion_solicitante.Transaction = trans;

                    //INSERTAR EN TABLA NACIONAL O IMPORTADO
                    if (obj.Tipo == "Solicitante")
                    {
                        Solicitante s = (Solicitante)obj;
                        comando_insercion_solicitante.CommandText = "insert into Solicitantes (Id, SolicitudesPendientes, Email, Nombre, Apellido, FechaNacimiento, Celular, listaProyectoID) values(@id, @soli, @email, @nom, @ape, @fech, @celu, @listProyID);";
                        comando_insercion_solicitante.Parameters.AddWithValue("@id", id_generado);
                        comando_insercion_solicitante.Parameters.AddWithValue("@soli", s.SolicitudesPendientes);
                        comando_insercion_solicitante.Parameters.AddWithValue("@email", s.Email);
                        comando_insercion_solicitante.Parameters.AddWithValue("@nom", s.Nombre);
                        comando_insercion_solicitante.Parameters.AddWithValue("@ape", s.Apellido);
                        comando_insercion_solicitante.Parameters.AddWithValue("@fech", s.FechaNac);
                        comando_insercion_solicitante.Parameters.AddWithValue("@celu", s.Celular);
                        comando_insercion_solicitante.Parameters.AddWithValue("@listProyID", id_generado); //Numero hardcodeado
                        //comando_insercion_solicitante.Parameters.AddWithValue("@listProyID", s.ListaProyectos);

                        comando_insercion_solicitante.ExecuteNonQuery(); //3)b) EJECUTO EL COMANDO
                    }

                    trans.Commit(); // 4) SI LLEGUÉ HASTA ACÁ, CONFIRMO TODAS LAS EJECUCIONES ANTERIORES

                    ret = true;
                }
                catch
                {
                    if (trans != null) trans.Rollback(); // 4)alternativo) ALGO SALIÓ MAL -> DESHAGO LAS EJECUCIONES
                    ret = false;
                    //throw;
                    //Comento el throw porque no encontre otra manera de que no explotque cuando quiero ingresar elementos repetidos
                    //Retorno falso y esto lo handleo en el front, muestro mensaje de error de que algunos elementos no son unicos
                }
                finally
                {
                    if (con.State == ConnectionState.Open) con.Close();
                }
            }

            return ret;
        }

        public bool Baja(int id)
        {
            bool ret = false;
            Usuario u = BuscarPorId(id);

            SqlConnection con = ObtenerConexion();
            string sql = "delete from Usuarios where Id=@id";
            SqlCommand com = new SqlCommand(sql, con);
            com.Parameters.AddWithValue("@id", id);

            SqlTransaction trans = null;

            try
            {
                if (u.Tipo == "Admin")
                {
                    con.Open();
                    com.ExecuteNonQuery();
                    con.Close();

                    ret = true;

                }

                if (u.Tipo == "Solicitante") //ACA HAY QUE VER SI SE HACE ASI O CON TIPO()
                {

                    con.Open();
                    trans = con.BeginTransaction();
                    SqlCommand comsoli = new SqlCommand();
                    comsoli.Connection = con;
                    comsoli.Transaction = trans;


                    Solicitante soli = (Solicitante)u;
                    comsoli.CommandText = "DELETE FROM Solicitantes where Id=@id;";
                    comsoli.Parameters.AddWithValue("@id", soli.Id);

                    comsoli.ExecuteNonQuery(); //3)b) EJECUTO EL COMANDO
                    trans.Commit();
                    com.ExecuteNonQuery();
                    ret = true;
                }


                // 4) SI LLEGUÉ HASTA ACÁ, CONFIRMO TODAS LAS EJECUCIONES ANTERIORES

            }
            catch
            {
                if (trans != null) trans.Rollback(); // 4)alternativo) ALGO SALIÓ MAL -> DESHAGO LAS EJECUCIONES 
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return ret;
        }

        public Usuario BuscarPorId(int id)
        {
            SqlConnection con = ObtenerConexion();
            string sql = "select * " +
                         "from Usuarios u, Solicitantes s " +
                         "where u.Tipo = 'Solicitante' and u.Id = s.Id and u.Id =" + id + "or u.Tipo ='Admin' and u.Id=" + id;
            SqlCommand com = new SqlCommand(sql, con);
            Usuario Usu = null;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {
                    string tipo = reader.GetString(3);
                    if (tipo == "Admin")
                    {
                        Usu = Auxiliar.CrearUsuario(reader);
                    }
                    else if (tipo == "Solicitante")
                    {
                        Usu = Auxiliar.CrearSolicitante(reader);
                        //return Usu;
                    }
                    return Usu;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return Usu;
        }

        public bool Modificacion(Usuario obj)
        {
            bool ret = false;

            if (obj != null)
            {
                SqlConnection con = ObtenerConexion();

                string sql_producto = "update Usuarios set Cedula=@ced, Pass=@pass, Tipo=@tipo where Id=@id;";
                //sql_producto += "select cast(scope_identity() as int);";

                SqlCommand comando_actualizacion_usuario = new SqlCommand(sql_producto, con);

                comando_actualizacion_usuario.Parameters.AddWithValue("@ced", obj.Cedula);
                comando_actualizacion_usuario.Parameters.AddWithValue("@pass", obj.Pass);
                comando_actualizacion_usuario.Parameters.AddWithValue("@tipo", obj.Tipo);
                comando_actualizacion_usuario.Parameters.AddWithValue("@id", obj.Id);
                SqlTransaction trans = null;

                try
                {
                    con.Open();
                    trans = con.BeginTransaction(); // 1) INICIAR / OBTENER LA TRANSACCIÓN
                    comando_actualizacion_usuario.Transaction = trans; // 2) ASIGNARLA AL COMANDO

                    //INSERTAR EN TABLA PRODUCTO Y OBTENER EL ID AUTOGENERADO
                    comando_actualizacion_usuario.ExecuteNonQuery();

                    SqlCommand comando_actualizacion_solicitante = new SqlCommand();
                    comando_actualizacion_solicitante.Connection = con;
                    comando_actualizacion_solicitante.Transaction = trans;

                    //INSERTAR EN TABLA NACIONAL O IMPORTADO
                    if (obj.Tipo == "Solicitante")
                    {
                        Solicitante s = (Solicitante)obj;
                        comando_actualizacion_solicitante.CommandText = "update Solicitantes set SolicitudesPendientes=@soli, Email=@email, Nombre=@nom, Apellido=@ape, FechaNacimiento=@fech, Celular=@celu where Id=@id;";
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@id", obj.Id);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@soli", s.SolicitudesPendientes);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@email", s.Email);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@nom", s.Nombre);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@ape", s.Apellido);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@fech", s.FechaNac);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@celu", s.Celular);
                        comando_actualizacion_solicitante.Parameters.AddWithValue("@listProyID", obj.Id); //Numero hardcodeado
                        //ACA NO ESTOY HACIENDO UPDATE DE LO DE LA LISTA DE PROYID HAY QUE HACERLO?

                        comando_actualizacion_solicitante.ExecuteNonQuery(); //3)b) EJECUTO EL COMANDO
                    }

                    trans.Commit(); // 4) SI LLEGUÉ HASTA ACÁ, CONFIRMO TODAS LAS EJECUCIONES ANTERIORES

                    ret = true;
                }
                catch
                {
                    if (trans != null) trans.Rollback(); // 4)alternativo) ALGO SALIÓ MAL -> DESHAGO LAS EJECUCIONES 
                    throw;
                }
                finally
                {
                    if (con.State == ConnectionState.Open) con.Close();
                }
            }

            return ret;
        }

        public List<Usuario> TraerTodo()
        {
            throw new NotImplementedException();
        }

        public List<Solicitante> TraerTodosLosSolicitantes()
        {
            List<Solicitante> solicitantes = new List<Solicitante>();

            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Usuarios u, Solicitantes s " +
                         "where u.Tipo = 'Solicitante' and " +
                         "u.Id = s.Id;";

            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                while (reader.Read())
                {

                    Solicitante soli = Auxiliar.CrearSolicitante(reader);

                    if (soli.Tipo == "Solicitante") solicitantes.Add(soli); //Aca estoy haciendo prueba para que solo me de solicitantes //Es necesario dejar el if? la query lo filtra antes
                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return solicitantes;
        }

        public List<Usuario> TraerTodosLosAdmins()
        {
            List<Usuario> usuarios = new List<Usuario>();

            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Usuarios " +
                         "where Tipo = 'Admin'";

            SqlCommand com = new SqlCommand(sql, con);

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();


                while (reader.Read())
                {

                    Usuario usu = Auxiliar.CrearUsuario(reader);

                    usuarios.Add(usu);
                }

                con.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }

            return usuarios;
        }



        public Usuario BuscarPorCedula(int cedula)
        {
            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Usuarios " +
                         "where Cedula =" + cedula +";";

            SqlCommand com = new SqlCommand(sql, con);
            Usuario Usu = null;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {
                    string tipo = reader.GetString(3);
                    if (tipo == "Admin")
                    {
                        Usu = Auxiliar.CrearUsuario(reader);
                    }
                    else if (tipo == "Solicitante")
                    {
                        Usu = Auxiliar.CrearUsuario(reader);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return Usu;
        }

        public Solicitante BuscarSolicitantePorCedula(int cedula)
        {
            SqlConnection con = ObtenerConexion();

            string sql = "select * " +
                         "from Usuarios u, Solicitantes s " +
                         "where u.Id = s.Id and " +
                         "u.Cedula =" + cedula + ";";

            SqlCommand com = new SqlCommand(sql, con);
            Solicitante soli = null;
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {

                   soli = Auxiliar.CrearSolicitante(reader);

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return soli;
        }


        //Juan 
        public bool SolicitudesPendientes(int id)
        {
            SqlConnection con = ObtenerConexion();
            bool resultado = false;

            string sql = "select s.SolicitudesPendientes from Solicitantes as s where id = " + id ;

            SqlCommand com = new SqlCommand(sql, con);
            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                //reader.Read();
                if (reader.Read())
                {

                    resultado = (bool)reader.GetBoolean(0);

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open) con.Close();
            }
            return resultado;
        }






        // se creo este metodo pero existe buscar por Id que solo es casterlo para poder acceder, creo que queda obsolteto 

        //public Solicitante BuscarSolicitantePorId(int id)
        //{
        //    SqlConnection con = ObtenerConexion();

        //    string sql = "select * " +
        //                 "from Usuarios u, Solicitantes s " +
        //                 "where u.Id = s.Id and " +
        //                 "u.Id =" + id + ";";

        //    SqlCommand com = new SqlCommand(sql, con);
        //    Solicitante soli = null;
        //    try
        //    {
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();

        //        //reader.Read();
        //        if (reader.Read())
        //        {

        //            soli = CrearSolicitante(reader);

        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (con.State == ConnectionState.Open) con.Close();
        //    }
        //    return soli;
        //}

    }
}
