﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Repositorios;

namespace Repositorios
{
    public class Auxiliar : Repositorio
    {

        //*  AUXILIAR PARA USUARIOS   //*


        public static Usuario CrearUsuario(SqlDataReader reader)
        {
            Usuario usu = new Usuario
            {

                Id = reader.GetInt32(0),
                Cedula = reader.GetInt32(1),
                Pass = reader.GetString(2),
                Tipo = reader.GetString(3)

            };
            return usu;
        }

        public static Solicitante CrearSolicitante(SqlDataReader reader)
        {
            // no van datos de proyectos!!!!
            List<Proyecto> proyectos = new List<Proyecto>();

            //List<Proyecto> proyectos = FachadaPrestamos.listaDeProyectosDeUnCliente(reader.GetInt32(0));

            Solicitante soli = new Solicitante
            {
                Id = reader.GetInt32(0),
                Cedula = reader.GetInt32(1),
                Pass = reader.GetString(2),
                Tipo = reader.GetString(3),
                SolicitudesPendientes = reader.GetBoolean(5),
                Email = reader.GetString(6),
                Nombre = reader.GetString(7),
                Apellido = reader.GetString(8),
                FechaNac = reader.GetDateTime(9),
                Celular = reader.GetString(10),
                ListaProyectos = proyectos
                //En verdad aca tendria que traer el id y con ese id llamo a la lista de proyectos que 
                //Corresponda pero no se

                //Hardodear aca tambien

                //En este caso es el 9 y no el 8 porque nuestra consulta devuelve en el 8
                //La primary key de la tabla solicitantes
            };
            return soli;
        }



        //*    AUXILIAR DE PROYECTOS     *//

        public static Cooperativo CrearCooperativo(SqlDataReader reader)
        {

            //Solicitante soli = FachadaPrestamos.buscarSoliporID(reader.GetInt32(15));
            //VER QUE HACEMOS ACA
            Solicitante soli = new Solicitante();


            Cooperativo coop = new Cooperativo
            {

                id = reader.GetInt32(0),
                titulo = reader.GetString(1),
                descripcion = reader.GetString(2),
                montoSolicitado = reader.GetDecimal(3),
                cantidadCuotas = reader.GetInt32(4),
                urlImagen = reader.GetString(5),
                tasaInteres = reader.GetDecimal(6),
                estado = reader.GetString(7),
                montoMaximo = reader.GetDecimal(8),
                fechaPresentacion = reader.GetDateTime(9),
                montoCuota = reader.GetDecimal(10),
                montoFinalConInte = reader.GetDecimal(11),
                tipo = reader.GetString(12),
                cantIntegrantes = reader.GetInt32(14),
                Solicitante = soli




            };
            return coop;
        }


        public static Personal CrearPersonal(SqlDataReader reader)
        {
            Solicitante soli = new Solicitante();
            //Solicitante soli = FachadaPrestamos.buscarSoliporID(reader.GetInt32(15));
            //VER QUE HACEMOS ACA

            Personal per = new Personal
            {
                id = reader.GetInt32(0),
                titulo = reader.GetString(1),
                descripcion = reader.GetString(2),
                montoSolicitado = reader.GetDecimal(3),
                cantidadCuotas = reader.GetInt32(4),
                urlImagen = reader.GetString(5),
                tasaInteres = reader.GetDecimal(6),
                estado = reader.GetString(7),
                montoMaximo = reader.GetDecimal(8),
                fechaPresentacion = reader.GetDateTime(9),
                montoCuota = reader.GetDecimal(10),
                montoFinalConInte = reader.GetDecimal(11),
                tipo = reader.GetString(12),
                descripcionPersonal = reader.GetString(13),
                Solicitante = soli

            };
            return per;
        }





    }
}
