﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorios;
using Dominio;
using System.IO;
using WebObligatorio.ViewModels;
using System.Configuration;

namespace WebObligatorio.Controllers
{
    public class UsuarioController : Controller
    {
        [HttpGet]
        public ActionResult Index(string mensaje)
        {
            if (Session["cedula"] != null)
            {
                return Redirect("/Usuario/Loguear");
            }
            else
            {
                ViewBag.Mensaje2 = mensaje;
                return View();
            }
        }

        [HttpPost]
        public ActionResult Index(int cedula, string pass)
        {
            if (cedula > 0 && pass.Length != 0)
            {
                bool validarCedulaEntre7y8Caracteres = Dominio.Usuario.ValidarCedula(cedula);
                if (validarCedulaEntre7y8Caracteres)
                {
                    Usuario usuario = FachadaPrestamos.buscarUsuarioPorCedula(cedula);
                    if (usuario != null)
                    {
                        if (usuario.Pass == pass)
                        {
                            Session["cedula"] = usuario.Cedula;
                            Session["tipo"] = usuario.Tipo;
                            Session["id"] = usuario.Id;
                           
                            return Redirect("/Usuario/Loguear");
                        }
                        else
                        {
                            ViewBag.mensaje = "La contraseña es incorrecta";
                        }
                    }
                    else
                    {
                        ViewBag.mensaje = "No hay usuario con esa cedula.";
                    }
                }
                else
                {
                    ViewBag.mensaje = "Ingrese una cedula valida entre 7 y 8 caracteres (Sin espacios ni guiones).";
                }

            }
            else
            {
                ViewBag.mensaje = "Todos los campos son requeridos.";
            }

            return View();
        }

        public ActionResult Loguear()

        {

       
            ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
            if (Session["cedula"] == null)
            {
                return Redirect("/usuario/Index");
            }
            else
            {
                if ((string)Session["tipo"] == "Solicitante")
                {
                    int idCliente = (int)Session["id"];
                    List<Proyecto> proyectos = FachadaPrestamos.listaDeProyectosDeUnCliente(idCliente);
                    return View(proyectos);
                }
                else
                {

                    return Redirect("/usuario/ListadoPendientes");
                }

            }
        }

        //Aca agregue view model en lugar de un solicitante para poder validar que 
        //Contraseña y verificar contraseña sean iguales con validaciones desde viewmodel
        //Y separar las validaciones de campos de nuestro dominio
        [HttpGet]
        public ActionResult Alta()
        {
            //Solicitante nuevoSoli = new Solicitante();
            //return View(nuevoSoli);

            AltaSolicitanteViewModel vm = new AltaSolicitanteViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Alta(AltaSolicitanteViewModel vm)
        {
            if (vm != null)
            {
                bool celularValido = Dominio.Solicitante.ValidarFormatoCelular(vm.Celular);
                bool mailValido = Dominio.Solicitante.ValidarFormatoEmail(vm.Email);
                bool passValido = Dominio.Solicitante.ValidarFormatoPassword(vm.Pass);
                bool pass2Valido = Dominio.Solicitante.ValidarFormatoPassword(vm.Pass2);
                bool edadMayorA21 = Dominio.Solicitante.EdadMayorA21(vm.FechaNac);
                bool contraYVerificarContraIguales = Dominio.Solicitante.ContraYVerificarContraSonIguales(vm.Pass, vm.Pass2);
                bool validarCedulaEntre7y8Caracteres = Dominio.Usuario.ValidarCedula(vm.Cedula);

                if (celularValido && mailValido && passValido && pass2Valido && edadMayorA21 && contraYVerificarContraIguales && validarCedulaEntre7y8Caracteres)//Hacer todas las validaciones
                {
                    RepoUsuarios repoUsuario = new RepoUsuarios();

                    List<Proyecto> listaVacia = new List<Proyecto>();

                    bool exito = FachadaPrestamos.AltaSolicitante(vm.Cedula, vm.Nombre, vm.Apellido, vm.FechaNac, vm.Celular, vm.Email, vm.Pass, "Solicitante", false, listaVacia);
                    //Hardcodeado el tipo de solicitante porque siempre va a ser soli, solicitudes pendientes se inicializa en false y lista de proyectos vacia
                    if (exito)
                    {
                        return RedirectToAction("Index", new { mensaje = "Alta exitosa." });
                    }
                    else
                    {
                        //Aca viene si en la base de datos habia un unique
                        ViewBag.mensaje = "Asegurese de que la cedula y/o el email no esten registrados en la aplicacion.";
                    }

                }
                else
                {
                    //Agrega un mensaje para cada validacion es especial
                    if (!celularValido)
                    {
                        ViewBag.mensaje = "Verifique que el celular tenga el siguiente formato 09X YYYYY siendo X un numero del 1 al 9 y la Y uno del 0 al 9 (el espacio en blanco es obligatorio).\n";
                    }
                    if (!mailValido)
                    {
                        ViewBag.mensaje += "Verifique que el formato del email sea el correcto, no olvide arroba ni punto.\n";
                    }
                    if (!passValido || !pass2Valido)
                    {
                        ViewBag.mensaje += "Verifique que la contraseña contenga al menos 1 letra mayuscula, 1 minuscula, 1 digito y que al menos tenga 6 caracteres de largo.\n";
                    }
                    if (!edadMayorA21)
                    {
                        ViewBag.mensaje += "Debe ser mayor de 21 años para registrarse.\n";
                    }
                    if (!contraYVerificarContraIguales)
                    {
                        ViewBag.mensaje += "Contraseña y verificar contraseña no coinciden.\n";
                    }
                    if (!validarCedulaEntre7y8Caracteres)
                    {
                        ViewBag.mensaje += "Ingrese una cedula valida debe tener entre 7 y 8 caracteres numericos.\n";
                    }
                }
                return View(vm);
            }
            else
            {
                return Redirect("/Usuario/Loguear");
            }

        }


        [HttpGet]
        public ActionResult ListadoPendientes()
        {
            //Puse esto aca para que nos muestre siempre los proyectos al ingresar a la pag y que el admin lo firte si quiere
            ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
            List<Proyecto> p = FachadaPrestamos.TraerTodosLosProyectos();
            return View(p);
        }
        [HttpPost]
        public ActionResult ListadoPendientes(int? cedula, DateTime? fecha, string estado, string texto)
        {
            int ci = 0;
            DateTime Fecha = DateTime.MinValue;
            if (cedula != null)
            {
                ci = (int)cedula;
            }

            if (fecha != null)
            {
                Fecha = (DateTime)fecha;
                

            }
            ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
            string Estado = estado;
            string Texto = texto;
            List<Proyecto> p = FachadaPrestamos.BuscarProyectosPorFiltro(ci, Fecha, Estado, Texto);

            return View(p);
        }

        public ActionResult CerrarSesion()
        {
            Session["tipo"] = null;
            Session["cedula"] = null;
            Session["id"] = null;
            return RedirectToAction("index");
        }

    }
}