﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebObligatorio.ViewModels;
using Dominio;
using Repositorios;
using System.IO;
using System.Configuration;

namespace WebObligatorio.Controllers
{
    public class ProyectoController : Controller
    {
        // GET: Proyecto
        public ActionResult Index()
        {
          
            

            return View();
        }

        [HttpGet]
        public ActionResult AltaProyectoPersonal()
        {

            if (Session["tipo"] != null)
            {

                if (Session["UrlProyecto"] != null) {
                    string rutaAplication = HttpRuntime.AppDomainAppPath;
                    string nombreCarpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                    string rutaDirectorioImagenes = Path.Combine(rutaAplication, nombreCarpeta);
                    string nombreArchivo = (string)Session["UrlProyecto"];
                    string rutaCompletaArchivo = Path.Combine(rutaDirectorioImagenes, nombreArchivo);


                    FileInfo Archivo = new FileInfo(rutaCompletaArchivo);
                    if (Archivo.Exists)
                    {
                        Archivo.Delete();
                    }
                }


                return View();
            }


            return Redirect("/Usuario/Loguear");
        }

        [HttpPost]
        // Juan 
        public ActionResult AltaProyectoPersonal(AltaProyectoPersonal AP)
        {

            
                
            if (Session["tipo"] != null) {


                if ((string)Session["tipo"] == "Solicitante") {

                   int idUsuario =  (int)Session["id"];
                    
                    bool solicitudes = FachadaPrestamos.SolicitudesPendientes(idUsuario);


                    if (solicitudes == false)
                    {





                        decimal min = FachadaPrestamos.TraerMin();

                        if (AP.montoSolicitado < min)
                        {
                            ViewBag.Mensaje = "Monto Minimo es:" + min;
                            return View(AP);
                        }

                        if (AP.Archivo != null)
                        {
                            /// buscar los maximos para realizar calculos
                            decimal Maximo = FachadaPrestamos.TraerMax();
                            decimal TasaInteres = FachadaPrestamos.BuscarTasaDeInteresPorCuota(AP.cantidadCuotas);

                            // carga de imagen 
                            string rutaAplication = HttpRuntime.AppDomainAppPath;
                            string nombreCarpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                            string rutaDirectorioImagenes = Path.Combine(rutaAplication, nombreCarpeta);
                            string nombreArchivo = Session["id"] + AP.titulo + AP.Archivo.FileName;
                            string rutaCompletaArchivo = Path.Combine(rutaDirectorioImagenes, nombreArchivo);
                            AP.Archivo.SaveAs(rutaCompletaArchivo);
                            ////////////////////////////////////////
                            string Titulo = AP.titulo;
                            string Descripcion = AP.descripcion;
                            decimal MontoSolicitado = AP.montoSolicitado;
                            int CantidadCuotas = AP.cantidadCuotas;
                            string UrlImagen = nombreArchivo;
                            string Estado = "Pendiente de revisión";
                            string Tipo = "Personal";
                            DateTime FechaPresentacion = DateTime.Today;
                            string DescripcionPersonal = AP.descripcionPersonal;
                            Personal p = new Personal
                            {
                                titulo = Titulo,
                                cantidadCuotas = CantidadCuotas,
                                descripcion = Descripcion,
                                descripcionPersonal = DescripcionPersonal,
                                fechaPresentacion = FechaPresentacion,
                                montoSolicitado = MontoSolicitado,
                                tipo = Tipo,
                                estado = Estado,
                                montoMaximo = Maximo,
                                tasaInteres = TasaInteres,
                                urlImagen = UrlImagen


                            };
                            //bool alta = FachadaPrestamos.altaProyectoPersonal(titulo, descripcion, montoSolicitado, cantidadCuotas, urlImagen, estado, fechaPresentacion, idSolicitante, descripcionPersonal, tipo);
                            p.CalcularTotalConIntereses();
                            p.CalcularMontoCuota();

                            //ViewBag.Mensaje = "Alta Existosa";
                            //return View();
                            return RedirectToAction("ConfirmarAltaProyectoPersonal", "Proyecto", p);

                        }
                        else
                        {
                            ViewBag.Mensaje = "Cargar una Imagen";
                            return View(AP);
                        }


                    }
                    else {
                        ViewBag.Mensaje = "Tienes solicitudes pendientes espera a que sean aprobadas o rechazadas";
                        return View();
                    }


                } 

            }





            return Redirect("/Usuario/Loguear");
        }
        
        public ActionResult ConfirmarAltaProyectoPersonal(Personal p) {

            if (Session["tipo"] != null)
            {


                if ((string)Session["tipo"] == "Solicitante")
                {
                    ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                    TempData["Personal"] = p;
                    Session["UrlProyecto"] = p.urlImagen; 
                    return View(p);
                }
            }


            return Redirect("/Usuario/Loguear");
        }

        public ActionResult EnvioAltaPersonal() {
            

            if (Session["tipo"] != null)
            {


                if ((string)Session["tipo"] == "Solicitante")
                {
                    if (TempData["Personal"] != null)
                    {
                        Personal p = (Personal)TempData["Personal"];
                        int idSolicitante = (int)Session["id"];
                        bool alta = FachadaPrestamos.altaProyectoPersonal(p.titulo, p.descripcion, p.montoSolicitado, p.cantidadCuotas, p.urlImagen, p.estado, p.fechaPresentacion, idSolicitante, p.descripcionPersonal, p.tipo, p.tasaInteres, p.montoFinalConInte, p.montoMaximo, p.montoCuota);

                        Session["UrlProyecto"] = null;
                        ViewBag.Mensaje = "Alta Exitosa";
                        return View("AltaProyectoPersonal");
                    }
                }


            }


                    return Redirect("/Usuario/Loguear");
        }



        [HttpGet]
        public ActionResult AltaProyectoCooperativo()
        {
            if (Session["tipo"] != null)
            {
                if (Session["UrlProyecto"] != null)
                {
                    string rutaAplication = HttpRuntime.AppDomainAppPath;
                    string nombreCarpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                    string rutaDirectorioImagenes = Path.Combine(rutaAplication, nombreCarpeta);
                    string nombreArchivo = (string)Session["UrlProyecto"];
                    string rutaCompletaArchivo = Path.Combine(rutaDirectorioImagenes, nombreArchivo);


                    FileInfo Archivo = new FileInfo(rutaCompletaArchivo);
                    if (Archivo.Exists)
                    {
                        Archivo.Delete();
                    }
                }
                return View();
            }


            return Redirect("/Usuario/Loguear");

        }

        [HttpPost]
        //juan
        public ActionResult AltaProyectoCooperativo(AltaProyectoCooperativo AC)
        {
            ViewBag.Mensaje = "";

            if ((string)Session["tipo"] == "Solicitante")
            {

                int idUsuario = (int)Session["id"];

                bool solicitudes = FachadaPrestamos.SolicitudesPendientes(idUsuario);


                if (solicitudes == false)
                {
                    decimal min = FachadaPrestamos.TraerMin();
                    decimal Maximo = FachadaPrestamos.TraerMax();
                    decimal TasaInteres = FachadaPrestamos.BuscarTasaDeInteresPorCuota(AC.cantidadCuotas);
                    if (AC.montoSolicitado < min)
                    {
                        ViewBag.Mensaje = "Monto Minimo es:" + min;
                        return View(AC);
                    }
                    if (AC.Archivo != null)
                    {
                        string rutaAplication = HttpRuntime.AppDomainAppPath;
                        string nombreCarpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                        string rutaDirectorioImagenes = Path.Combine(rutaAplication, nombreCarpeta);
                        string nombreArchivo = Session["id"] + AC.titulo + AC.Archivo.FileName;
                        string rutaCompletaArchivo = Path.Combine(rutaDirectorioImagenes, nombreArchivo);
                        AC.Archivo.SaveAs(rutaCompletaArchivo);


                        string Titulo = AC.titulo;
                        string Descripcion = AC.descripcion;
                        decimal MontoSolicitado = AC.montoSolicitado;
                        int CantidadCuotas = AC.cantidadCuotas;
                        string UrlImagen = nombreArchivo;
                        string Estado = "Pendiente de revisión";
                        string Tipo = "Cooperativo";
                        DateTime FechaPresentacion = DateTime.Today;
                        int IdSolicitante = (int)Session["id"];
                        int CantidadIntegrantes = AC.cantIntegrantes;
                        Cooperativo c = new Cooperativo
                        {
                            titulo = Titulo,
                            descripcion = Descripcion,
                            montoSolicitado = MontoSolicitado,
                            cantidadCuotas = CantidadCuotas,
                            urlImagen = UrlImagen,
                            estado = Estado,
                            tipo = Tipo,
                            cantIntegrantes = CantidadIntegrantes,
                            fechaPresentacion = FechaPresentacion,
                            montoMaximo = Maximo,
                            tasaInteres = TasaInteres


                        };
                        c.CalcularTotalConIntereses();
                        c.CalcularMontoCuota();
                        return RedirectToAction("ConfirmarAltaProyectoCooperativo", "Proyecto", c);

                    }
                    else
                    {
                        ViewBag.Mensaje = "Cargar una Imagen";
                        return View(AC);
                    }


                }else
                {
                    ViewBag.Mensaje = "Tienes solicitudes pendientes espera a que sean aprobadas o rechazadas";
                    return View();
                }



                
            }

            return Redirect("/Usuario/Loguear");
        }

        //JUAN
        public ActionResult ConfirmarAltaProyectoCooperativo(Cooperativo c)
        {

            if (Session["tipo"] != null)
            {
                ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
                TempData["Cooperativo"] = c;
                Session["UrlProyecto"] = c.urlImagen;
                return View(c);
            }


            return Redirect("/Usuario/Loguear");

            
        }

        public ActionResult EnvioAltaCooperativo()
        {
            if (Session["tipo"] != null)
            {
                if (TempData["Cooperativo"] != null)
                {
                    Cooperativo c = (Cooperativo)TempData["Cooperativo"];
                    int idSolicitante = (int)Session["id"];
                    FachadaPrestamos.altaProyectoCooperativo(c.titulo, c.descripcion, c.montoSolicitado, c.cantidadCuotas, c.urlImagen, c.estado, c.fechaPresentacion, idSolicitante, c.cantidadCuotas, c.tipo, c.tasaInteres, c.montoFinalConInte, c.montoMaximo, c.montoCuota);
                }

                Session["UrlProyecto"] = null;
                ViewBag.Mensaje = "Alta Exitosa";
                return View("AltaProyectoCooperativo");
            }


            return Redirect("/Usuario/Loguear");


            
        }


        public ActionResult AprobarProyecto(int idProyecto)
        {
            string estado = "Aprobado";
            FachadaPrestamos.CambiarEstadoProyecto(estado, idProyecto);

            return Redirect("/usuario/ListadoPendientes");
        }

        public ActionResult RechazarProyecto(int idProyecto)
        {
            string estado = "Rechazado";
            FachadaPrestamos.CambiarEstadoProyecto(estado, idProyecto);

            return Redirect("/usuario/ListadoPendientes");
        }


        public ActionResult EvaluacionProyecto(int idProyecto) 
        {
            ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
            Proyecto elProyecto = FachadaPrestamos.buscarProyectoPorId(idProyecto);

            if (elProyecto.tipo == "Personal")
            {
                return View(elProyecto);
            }
            else if (elProyecto.tipo == "Cooperativo")
            {
                return RedirectToAction("/EvaluacionProyectoCooperativo", new{ idProyecto = idProyecto });
            }
            else
            {
                return Redirect("/usuario/Loguear");
            }

        }

        public ActionResult EvaluacionProyectoCooperativo(int idProyecto)
        {
            ViewBag.Carpeta = ConfigurationManager.AppSettings["CarpetaImagenes"];
            Proyecto elProyecto = FachadaPrestamos.buscarProyectoPorId(idProyecto);

            if (elProyecto.tipo == "Personal")
            {
                return Redirect("/proyecto/EvaluacionProyecto");
            }
            else if (elProyecto.tipo == "Cooperativo")
            {
                return View(elProyecto);
            }
            else
            {
                return Redirect("/usuario/Loguear");
            }

        }

        [HttpPost]
        public ActionResult EvaluacionProyecto(int idProyecto, int puntaje)
        {
            //Proyecto elProyecto = FachadaPrestamos.buscarProyectoPorId(idProyecto);

                if (puntaje >= 6)
                {
                    return RedirectToAction("/AprobarProyecto", new { idProyecto = idProyecto });
                }
                else 
                {
                    return RedirectToAction("/RechazarProyecto", new { idProyecto = idProyecto });
                }

        }

        [HttpPost]
        public ActionResult EvaluacionProyectoCooperativo(int idProyecto, int puntaje)
        {
            //Proyecto elProyecto = FachadaPrestamos.buscarProyectoPorId(idProyecto);


            if (puntaje >= 6)
            {
                return RedirectToAction("/AprobarProyecto", new { idProyecto = idProyecto });
            }
            else
            {
                return RedirectToAction("/RechazarProyecto", new { idProyecto = idProyecto });
            }

        }


    }
}

