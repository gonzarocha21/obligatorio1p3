﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace WebObligatorio.ViewModels
{
    public class AltaSolicitanteViewModel
    {
        [Required(ErrorMessage = "La cedula es requerida")]
        public int Cedula { get; set; }

        [Required(ErrorMessage = "La contrasea es requerida")]
        [DataType(DataType.Password), Display(Name = "Contraseña")]
        public string Pass { get; set; }
        
        [Required(ErrorMessage = "Confirmar contraseña es requerido")]
        [DataType(DataType.Password), Compare("Pass",
            ErrorMessage = "Las contraseñas no coincide"),
            Display(Name = "Confirmar contraseña")]
        public string Pass2 { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "El email es requerida")]
        public string Email { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El apellido es requerido")]
        public string Apellido { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de nacimiento")]
        [Required(ErrorMessage = "La fecha de nacimineto es requerida")]
        public DateTime FechaNac { get; set; }

        [Required(ErrorMessage = "El celular es requerido")]
        public string Celular { get; set; }
    }
}