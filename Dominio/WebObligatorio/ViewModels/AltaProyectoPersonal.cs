﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebObligatorio.ViewModels
{
    public class AltaProyectoPersonal
    {
        [Display(Name = "Titulo")]
        [Required(ErrorMessage = "El titulo es requerido")]
        public string titulo { get; set; }

        [Display(Name = "Descripcion")]
        [Required(ErrorMessage = "debe ingresar una descripcion")]
        public string descripcion { get; set; }

        [Display(Name = "Monto a Solicitar")]
        [Required(ErrorMessage = "Ingrese un monto 3")]
        public decimal montoSolicitado { get; set; }

        [Display(Name = "Cantidad de Cuotas")]
        [Required(ErrorMessage = "MontoSolicitado")]
        [Range(1, 16)]
        public int cantidadCuotas { get; set; }


        public HttpPostedFileBase Archivo { get; set; }



        ////Cooperativo 
        //[Display(Name = "Cantidad Integrantes")]
        //[Required(ErrorMessage = "Ingrese cantidad de integrantes")]
        //public int cantIntegrantes { get; set; }

        //Personal
        [Display(Name = "Descripcion Personal ")]
        [Required(ErrorMessage = "Ingrese una descripcion personal")]
        public string descripcionPersonal { get; set; }
    }
}