﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebObligatorio.ViewModels
{
    public class EvaluacionProyectoViewModel
    {
        public Proyecto Proyecto { get; set; }
        public int Puntaje { get; set; }
    }
}